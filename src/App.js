import React from 'react';
import './App.css';
import Sidebar from './sidebar';
import Coin from './coin';

function randomStartPosition() {

  // These are in %.
  const region = {
    left: 40,
    bottom: 15,
    width: 20,
    height: 7,
  };

  const left = region.left + Math.random() * region.width;
  const bottom = region.bottom - Math.random() * region.height;

  return [left, bottom];
}

function App() {
  const drop_region_ref = React.useRef(null);
  // We abuse a ref for this drag hint dialog because the Draggable react
  // component bugs out on calling state setters onStart.
  const drag_hint_ref = React.useRef(null);
  const [coins, setCoins] = React.useState(1);
  const addCoin = () => setCoins(coins + 1);


  let first_left, first_bottom;
  const coinElems = Array.from(Array(coins).keys()).map((key) => {
    const [left, bottom] = randomStartPosition();

    if (key === 0) {
      first_left = left;
      first_bottom = bottom;
    }
    return (
      <Coin
        key={key}
        dropRegionRef={drop_region_ref}
        dragHintRef={drag_hint_ref}
        startPos={{
          left: left,
          bottom: bottom,
        }}
      />
    );
  });

  return (
    <div className="app">
      <Sidebar addCoin={addCoin}/>
      <main>
        <div className="drop_region" ref={drop_region_ref}></div>
        {coinElems}
        <div
          className="drag_hint scale-up"
          style={{
            left: first_left - 4 + '%',
            bottom: first_bottom + 8 + '%',
          }}
          ref={drag_hint_ref}
        >
        Drag me!
        </div>
      </main>
    </div>
  );
}

export default App;
