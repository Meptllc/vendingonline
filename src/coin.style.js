import styled from "styled-components";

const CoinImg = styled.img`
  position: absolute;
  margin: 0;
  height: 100px;
  left: ${(props) => props.startPos.left}%;
  bottom: ${(props) => props.startPos.bottom}%;
`;

export default CoinImg;
