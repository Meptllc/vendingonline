import React from 'react';
import {
  CDBSidebar,
  CDBSidebarHeader,
  CDBSidebarMenu,
  CDBSidebarMenuContent,
} from 'cdbreact';
import './sidebar.css';

import ShoppingCart from './shoppingcart';
import coin_image from './images/mario-coin.png';


const Sidebar = (props) => {
  return (
    <div style={{ display: 'flex', height: '100vh', overflow: 'scroll initial' }}>
      <CDBSidebar textColor="#fff" backgroundColor="#333">
        <CDBSidebarHeader prefix={<i className="fa fa-bars fa-large"></i>}>
        </CDBSidebarHeader>
        <CDBSidebarMenu>
          <CDBSidebarMenuContent isShy='true'>
            <div className='price-display'>
              <img className='sidebar-coin' src={coin_image} alt=''/>
              <div className='multiplier'>x</div>
              <div>1</div>
              <div className='final-price'>$0.01</div>
            </div>
          </CDBSidebarMenuContent>
          <CDBSidebarMenuContent className='justify-center' isShy='true'>
            <ShoppingCart addCoin={props.addCoin}/>
          </CDBSidebarMenuContent>
        </CDBSidebarMenu>
      </CDBSidebar>
    </div>
  );
};

export default Sidebar;
